export interface Edge<A> {
  cursor: string
  node: A
}

export interface PageInfo {
  startCursor?: string,
  endCursor?: string
  hasNextPage: boolean
  hasPrevPage: boolean
}

export interface Connection<A> {
  edges: Array<Edge<A>>
  pageInfo: PageInfo
}
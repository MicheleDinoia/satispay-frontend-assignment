import { toConnection } from "../functions"

const as = [
  { id: '1', data: "test_data_1" },
  { id: '2', data: "test_data_2" },
  { id: '3', data: "test_data_3" },
  { id: '4', data: "test_data_4" },
  { id: '5', data: "test_data_5" },
  { id: '6', data: "test_data_6" },
]

describe('toConnection', () => {
  test('it returns the array sliced', () => {
      expect(toConnection(as, 3, false, false)).toEqual(expect.objectContaining(
          {
            edges: [
              {
                cursor: '1',
                node: {
                  data: 'test_data_1',
                  id: '1'
                }
              },
              {
                cursor: '2',
                node: {
                  data: 'test_data_2',
                  id: '2'
                }
              },
              {
                cursor: '3',
                node: {
                  data: 'test_data_3',
                  id: '3'
                }
              }
            ]
          }        
        )
      )
  })

  test('it returns the right endCursor and startCursor', () => {
      expect(toConnection(as, 3, false, false)).toEqual(expect.objectContaining(
        {
          pageInfo: expect.objectContaining({
            endCursor: '3',
            startCursor: '1'
          })
        }        
      )
    )
  })

  test('it returns the right hasNextPage and hasPrevPage', () => {
      expect(toConnection(as, 3, true, true)).toEqual(expect.objectContaining(
        {
          pageInfo: expect.objectContaining({
            hasNextPage: true,
            hasPrevPage: true,
          })
        }        
      )
    )
  })
})
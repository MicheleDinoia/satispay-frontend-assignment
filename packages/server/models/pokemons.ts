import { pipe } from "fp-ts/lib/pipeable";
import * as O from "fp-ts/lib/Option";
import * as A from "fp-ts/lib/Array";
import * as S from 'fp-ts/lib/Semigroup'
import { ordNumber } from 'fp-ts/lib/Ord'
import { identity } from "fp-ts/lib/function";
import { data } from "../data/pokemons";
import { toConnection } from "../functions";
import { Connection } from "../types";

export interface Pokemon {
  id: string;
  name: string;
  types: string[];
}

const SIZE = 10;

export function query(args: {
  after?: string;
  before?: string;
  limit?: number;
  q?: string;
  type?: string
}): Connection<Pokemon> {
  const { after, q, limit = SIZE, type, before } = args;

  const filterByQ: (as: Pokemon[]) => Pokemon[] =
    // filter only if q is defined
    q === undefined
      ? identity
      : A.filter(p => p.name.toLowerCase().includes(q.toLowerCase()));

  const filterByType: (as: Pokemon[]) => Pokemon[] =
  // filter only if type is defined
  type === undefined
    ? identity
    : A.filter( p =>
      pipe(
        p.types,
        A.map(a => a.toLowerCase().includes(type.toLowerCase())),
        b => S.fold(S.semigroupAny)(false, b)
      )
    );

  const sliceByAfter: (as: Pokemon[]) => Pokemon[] =
    // filter only if after is defined
    after === undefined
      ? identity
      : as =>
          pipe(
            as,
            A.findIndex(a => a.id === after),
            O.map(a => a + 1),
            O.fold(() => as, idx => as.slice(idx))
          );

  const sliceByBefore: (as: Pokemon[]) => Pokemon[] =
    // filter only if before is defined
    before === undefined
      ? identity
      : as =>
          pipe(
            as,
            A.findIndex(a => a.id === before),
            O.map(a => S.getJoinSemigroup(ordNumber).concat(a - limit, 0)),
            O.fold(() => as, idx => as.slice(idx)
            )
          );
          
 

  const resultsNotSliced: Pokemon[] = pipe(
    data,
    filterByQ,
    filterByType,
    // sliceByAfter,
    // sliceByBefore,
    // slicing limit + 1 because the `toConnection` function should known the connection size to determine if there are more results
    // slice(0, limit + 1) // check why this slice
  );

  const results: Pokemon[] = pipe (
    resultsNotSliced,
    sliceByAfter,
    sliceByBefore,
  )

  const hasNextPage = limit < results.length;
  const hasPrevPage = 
    resultsNotSliced.length > 0 
    ? pipe(
        results,
        A.findIndex(a => a.id === resultsNotSliced[0].id),
        O.fold(() => true, () => false)
      ) 
      : false
  return toConnection(results, limit, hasNextPage, hasPrevPage);
}

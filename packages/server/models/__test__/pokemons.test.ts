import { query } from '../pokemons'

jest.mock("../../data/pokemons", () => ({
  data: [
    {
      id: '1',
      name: 'test_name_1',
      types: [
        'test_first_type',
        'test_second_type'
      ],
    },
    {
      id: '2',
      name: 'test_name_2',
      types: [
        'test_second_type',
        'test_third_type',
      ],
    },
    {
      id: '3',
      name: 'test_name_3',
      types: [
        'test_third_type',
      ],
    }
  ]
}))

describe("models / pokemons", () => {
  describe('when all params are missing', () => {
    test('it returns the Edges and the pageInfo', () => {
      expect(query({})).toEqual(
        {
          "edges":[
             {
                "cursor":"1",
                "node":{
                   "id":"1",
                   "name":"test_name_1",
                   "types":[
                    'test_first_type',
                    'test_second_type'
                   ]
                }
             },
             {
                "cursor":"2",
                "node":{
                   "id":"2",
                   "name":"test_name_2",
                   "types":[
                    'test_second_type',
                    'test_third_type',
                   ]
                }
             },
             {
                "cursor":"3",
                "node":{
                   "id":"3",
                   "name":"test_name_3",
                   "types":[
                      'test_third_type',
                   ]
                }
             },
          ],
          "pageInfo":{
             "endCursor":"3",
             "hasNextPage":false,
             "hasPrevPage":false,
             "startCursor":"1"
          }
       }
      )
    }) 
  })

  describe('when q param is defined', () => {
    test('it returns the Edges and the pageInfo filtered by q', () => {
      expect(query({q: "test_name_3"})).toEqual(
        {
          "edges":[
             {
                "cursor":"3",
                "node":{
                   "id":"3",
                   "name":"test_name_3",
                   "types":[
                      'test_third_type',
                   ]
                }
             },
          ],
          "pageInfo":{
             "endCursor": "3",
             "hasNextPage": false,
             "hasPrevPage": false,
             "startCursor": "3"
          }
       }
      )
    }) 
  })

  describe('when type param is defined', () => {
    test('it returns the Edges and the pageInfo filtered by type', () => {
      expect(query({ type: "test_second_type" })).toEqual(
        {
          "edges":[
            {
              "cursor":"1",
              "node":{
                 "id":"1",
                 "name":"test_name_1",
                 "types":[
                  'test_first_type',
                  'test_second_type'
                 ]
              }
            },
            {
                "cursor":"2",
                "node":{
                  "id":"2",
                  "name":"test_name_2",
                  "types":[
                    'test_second_type',
                    'test_third_type',
                  ]
                }
            },
          ],
          "pageInfo":{
             "endCursor": "2",
             "hasNextPage": false,
             "hasPrevPage": false,
             "startCursor": "1"
          }
       }
      )
    }) 
  })

  describe('when limit param is defined', () => {
    test('it returns elements <= limit', () => {
      expect(query({ limit: 2 })).toEqual(
        {
          "edges":[
            {
              "cursor":"1",
              "node":{
                 "id":"1",
                 "name":"test_name_1",
                 "types":[
                  'test_first_type',
                  'test_second_type'
                 ]
              }
            },
            {
              "cursor":"2",
              "node":{
                "id":"2",
                "name":"test_name_2",
                "types":[
                  'test_second_type',
                  'test_third_type',
                ]
              }
            },
          ],
          "pageInfo":{
             "endCursor": "2",
             "hasNextPage": true,
             "hasPrevPage": false,
             "startCursor": "1"
          }
       }
      )
    }) 
  })

  describe('when after param is defined', () => {
    test('it returns the next elemets after the param', () => {
      expect(query({ after: "2" })).toEqual(
        {
          "edges":[
            {
              "cursor":"3",
              "node":{
                "id":"3",
                "name":"test_name_3",
                "types":[
                  'test_third_type',
                ]
              }
            },
          ],
          "pageInfo":{
             "endCursor": "3",
             "hasNextPage": false,
             "hasPrevPage": true,
             "startCursor": "3"
          }
       }
      )
    }) 
  })

  describe('when before param is defined', () => {
    test('it returns n = limit elemets before the param', () => {
      expect(query({ before: "2", limit: 2 })).toEqual(
        {
          "edges":[
            {
              "cursor":"1",
              "node":{
                 "id":"1",
                 "name":"test_name_1",
                 "types":[
                  'test_first_type',
                  'test_second_type'
                 ]
              }
            },
            {
              "cursor":"2",
              "node":{
                "id":"2",
                "name":"test_name_2",
                "types":[
                  'test_second_type',
                  'test_third_type',
                ]
              }
            },
          ],
          "pageInfo":{
             "endCursor": "2",
             "hasNextPage": true,
             "hasPrevPage": false,
             "startCursor": "1"
          }
       }
      )
    }) 
  })

  describe('if total elemets < limit', () => {
    test('it returns hasNextPage = true', () => {
      expect(query({ limit: 2 })).toEqual(
        {
          "edges":[
            {
              "cursor":"1",
              "node":{
                 "id":"1",
                 "name":"test_name_1",
                 "types":[
                  'test_first_type',
                  'test_second_type'
                 ]
              }
            },
            {
                "cursor":"2",
                "node":{
                  "id":"2",
                  "name":"test_name_2",
                  "types":[
                    'test_second_type',
                    'test_third_type',
                  ]
                }
            },
          ],
          "pageInfo":{
             "endCursor": "2",
             "hasNextPage": true,
             "hasPrevPage": false,
             "startCursor": "1"
          }
       }
      )
    })
  })
})
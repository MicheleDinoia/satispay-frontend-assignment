import React from 'react';
import { Layout, Typography } from 'antd';
const { Header } = Layout;
const { Title } = Typography;

export default function AppHeader() {
  return (
    <Header style={{ textAlign: 'center' }}>
      <Title style={{ color: '#fff', margin: '0', height: '100%' }} level={1}>
        Pokemons
      </Title>
    </Header>
  );
}

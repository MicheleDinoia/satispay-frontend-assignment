import React from 'react';
import { Typography, Row, Col } from 'antd';

const { Title } = Typography;

export default function ErrorPage() {
  return (
    <Row data-testid="generic-error">
      <Col span={8} offset={8} style={{ textAlign: 'center' }}>
        <Title level={3}>(▰︶︹︺▰)</Title>
        <Title level={3}>Oh no there was an Error!</Title>
        <Title level={3}>Please try later. </Title>
      </Col>
    </Row>
  );
}

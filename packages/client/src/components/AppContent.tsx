import React from 'react';
import { Layout } from 'antd';
import PokemonList from '@components/PokemonList';
const { Content } = Layout;

export default function AppContent() {
  return (
    <Content>
      <PokemonList />
    </Content>
  );
}

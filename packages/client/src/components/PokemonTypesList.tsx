import React from 'react';
import PokemonType from '@components/PokemonType';

export type PokemonTypesListProps = {
  types: string[];
};

export default function PokemonTypesList({ types }: PokemonTypesListProps) {
  return (
    <>
      {types.map((type: string, index: number) => (
        <PokemonType type={type} key={index}></PokemonType>
      ))}
    </>
  );
}

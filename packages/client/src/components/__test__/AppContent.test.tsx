import React from 'react';
import AppContent from '../AppContent';
import { render } from '@testing-library/react';

jest.mock('@components/PokemonList', () => () => <div>POKEMON LIST</div>);

describe('AppContent', () => {
  test('it renders properly', () => {
    const { container } = render(<AppContent></AppContent>);
    expect(container).toMatchSnapshot();
  });
});

import '@root/mocks/matchMedia.mock';
import React from 'react';
import GenericErrorPage from '@components/GenericErrorPage';
import { render } from '@testing-library/react';

describe('GenericErrorPage', () => {
  test('it renders properly', () => {
    const { container } = render(<GenericErrorPage />);
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
import PokemonType from '@components/PokemonType';
import type { PokemonTypeProps } from '@components/PokemonType';
import { render } from '@testing-library/react';

describe('PokemonType', () => {
  test('it renders properly', () => {
    const props: PokemonTypeProps = { type: 'test_type' };
    const { getByText } = render(<PokemonType {...props}></PokemonType>);
    expect(getByText('TEST_TYPE')).toBeTruthy();
  });
});

import React from 'react';
import PokemonTypesList from '@components/PokemonTypesList';
import type { PokemonTypesListProps } from '@components/PokemonTypesList';
import { render } from '@testing-library/react';

describe('PokemonType', () => {
  test('it renders properly', () => {
    const props: PokemonTypesListProps = {
      types: ['test_type_1', 'test_type_2', 'test_type_3']
    };
    const { getByText } = render(<PokemonTypesList {...props}></PokemonTypesList>);
    expect(getByText('TEST_TYPE_1')).toBeTruthy();
    expect(getByText('TEST_TYPE_2')).toBeTruthy();
    expect(getByText('TEST_TYPE_3')).toBeTruthy();
  });
});

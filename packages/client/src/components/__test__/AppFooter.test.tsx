import React from 'react';
import AppFooter from '@components/AppFooter';
import { render } from '@testing-library/react';

describe('AppFooter', () => {
  test('it renders properly', () => {
    const { container } = render(<AppFooter></AppFooter>);
    expect(container).toMatchSnapshot();
  });
});

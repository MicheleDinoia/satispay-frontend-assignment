import React from 'react';
import AppHeader from '@components/AppHeader';
import { render } from '@testing-library/react';

describe('AppHeader', () => {
  test('it renders properly', () => {
    const { container } = render(<AppHeader></AppHeader>);
    expect(container).toMatchSnapshot();
  });
});

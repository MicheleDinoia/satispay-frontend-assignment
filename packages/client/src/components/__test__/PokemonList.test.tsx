import '@root/mocks/matchMedia.mock';
import React from 'react';
import PokemonList from '@components/PokemonList';
import { render, act, fireEvent } from '@testing-library/react';
import { POKEMONS_QUERY } from '@root/QraphQL/Queries';
import { MockedProvider } from '@apollo/client/testing';

const firstQueryResult = {
  pokemons: {
    pageInfo: {
      endCursor: 'test_end_cursor',
      startCursor: 'test_start_cursor',
      hasPrevPage: true,
      hasNextPage: true
    },
    edges: [
      {
        node: {
          id: '011',
          name: 'Metapod',
          classification: 'Cocoon Pokémon',
          types: ['Bug']
        }
      }
    ]
  }
};

describe('PokemonList', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('when query completes', () => {
    test('it renders properly ', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: firstQueryResult
          }
        }
      ];
      await act(async () => {
        const { getByTestId, getByText } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByTestId('search-name')).toBeTruthy();
        expect(getByTestId('search-type')).toBeTruthy();
        expect(getByTestId('pokemon-table')).toBeTruthy();
        expect(getByTestId('button-prev')).toBeTruthy();
        expect(getByTestId('button-next')).toBeTruthy();
        expect(getByText('Metapod')).toBeTruthy();
        expect(getByText('Cocoon Pokémon')).toBeTruthy();
        expect(getByText('BUG')).toBeTruthy();
      });
    });
  });

  describe('when query return an error', () => {
    test('it shows the error component', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          error: new Error('An error occurred')
        }
      ];
      await act(async () => {
        const { getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByTestId('generic-error')).toBeTruthy();
      });
    });
  });

  describe('when the user search a pokemon name', () => {
    test('it shows the new data', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: { ...firstQueryResult }
          }
        },
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: 'test_name', type: '', after: '', before: '' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: true,
                  hasNextPage: true
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_name',
                      classification: 'test_classification',
                      types: ['test_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { container, getByText, getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        fireEvent.change(getByTestId('search-name'), {
          target: { value: 'test_name' }
        });
        await new Promise((resolve) => setTimeout(resolve, 0));
        fireEvent.click(container.querySelector('.PokeomonList__input-name button') as Element);
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByText('test_name')).toBeTruthy();
        expect(getByText('test_classification')).toBeTruthy();
        expect(getByText('TEST_TYPE')).toBeTruthy();
      });
    });
  });

  describe('when the user filter a pokemon by type', () => {
    test('it shows the new data', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: { ...firstQueryResult }
          }
        },
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: 'changed_type', after: '', before: '' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: true,
                  hasNextPage: true
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_name',
                      classification: 'test_classification',
                      types: ['changed_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { container, getByText, getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        fireEvent.change(getByTestId('search-type'), {
          target: { value: 'changed_type' }
        });
        await new Promise((resolve) => setTimeout(resolve, 0));
        fireEvent.click(container.querySelector('.PokeomonList__input-type button') as Element);
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByText('test_name')).toBeTruthy();
        expect(getByText('test_classification')).toBeTruthy();
        expect(getByText('CHANGED_TYPE')).toBeTruthy();
      });
    });
  });

  describe('when the user clicks on prev page', () => {
    test('it shows the new data', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: { ...firstQueryResult }
          }
        },
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: 'test_start_cursor' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: true,
                  hasNextPage: true
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_prev_page_pokemon',
                      classification: 'test_classification',
                      types: ['changed_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { getByText, getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        fireEvent.click(getByTestId('button-prev'));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByText('test_prev_page_pokemon')).toBeTruthy();
      });
    });
  });

  describe('when the user clicks on next page', () => {
    test('it shows the new data', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: { ...firstQueryResult }
          }
        },
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: 'test_end_cursor', before: '' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: true,
                  hasNextPage: true
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_next_page_pokemon',
                      classification: 'test_classification',
                      types: ['changed_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { getByText, getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        fireEvent.click(getByTestId('button-next'));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByText('test_next_page_pokemon')).toBeTruthy();
      });
    });
  });

  describe('if there are no previous pages', () => {
    test('the user can not click the previous button', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: false,
                  hasNextPage: true
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_name',
                      classification: 'test_classification',
                      types: ['changed_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByTestId('button-prev')).toBeDisabled();
      });
    });
  });

  describe('if there are no next pages', () => {
    test('the user can not click the next button', async () => {
      const mocks = [
        {
          request: {
            query: POKEMONS_QUERY,
            variables: { q: '', type: '', after: '', before: '' }
          },
          result: {
            data: {
              pokemons: {
                pageInfo: {
                  endCursor: 'test_end_cursor',
                  startCursor: 'test_start_cursor',
                  hasPrevPage: false,
                  hasNextPage: false
                },
                edges: [
                  {
                    node: {
                      id: '001',
                      name: 'test_name',
                      classification: 'test_classification',
                      types: ['changed_type']
                    }
                  }
                ]
              }
            }
          }
        }
      ];
      await act(async () => {
        const { getByTestId } = render(
          <MockedProvider mocks={mocks} addTypename={false}>
            <>
              <PokemonList />
              );
            </>
          </MockedProvider>
        );
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(getByTestId('button-next')).toBeDisabled();
      });
    });
  });

  // test('it calls useQuery the first time with empty parameters', () => {
  //   // (useQuery as jest.Mock).mockReturnValue({ error: false, loading: false, data: queryResult });
  //   await act(async () => {
  //     setup(mocksSuccess);
  //     await new Promise(resolve => setTimeout(resolve, 0));
  //     expect(useQuery).toHaveBeenCalledWith("TEST_QUERY", { "variables": { "after": "", "before": "", "q": "", "type": "" } })
  //   }
  // })

  // test('it calls useQuery the first time with empty parameters', () => {
  //   (useQuery as jest.Mock).mockReturnValue({ error: false, loading: false, data: queryResult });
  //   render(<PokemonList></PokemonList>);
  //   expect(useQuery).toHaveBeenCalledWith("TEST_QUERY", { "variables": { "after": "", "before": "", "q": "", "type": "" } })
  // })
});

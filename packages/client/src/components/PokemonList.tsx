import React, { useEffect, useState, useCallback } from 'react';
import { Row, Col, Input } from 'antd';
import { Table, Space, Button } from 'antd';
import { useQuery } from '@apollo/client';
import { POKEMONS_QUERY } from '@root/QraphQL/Queries';
import PokemonTypesList from '@components/PokemonTypesList';
import type { PokemonsQuery } from '@root/models/Pokemons';
import GenericErrorPage from '@components/GenericErrorPage';

const pokemonColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: 'Types',
    dataIndex: 'types',
    key: 'types',
    render: (types: string[]) => <PokemonTypesList types={types}></PokemonTypesList>
  },
  {
    title: 'Classification',
    dataIndex: 'classification',
    key: 'classification'
  }
];
type TableData = { key: string; name: string; types: string[]; classification: string }[];
const paginationEmptyValue = { before: '', after: '' };

export default function PokemonList() {
  const [pagination, setPagination] = useState(paginationEmptyValue);

  const [name, setName] = useState('');
  const [type, setType] = useState('');
  const [tableData, setTableData] = useState<TableData>([]);
  const { loading, error, data } = useQuery<PokemonsQuery>(POKEMONS_QUERY, {
    variables: { q: name, after: pagination.after, type, before: pagination.before }
  });

  const goNextPage = useCallback(() => {
    data && setPagination({ after: data.pokemons.pageInfo.endCursor, before: '' });
  }, [data]);

  const goPrevPage = useCallback(() => {
    data && setPagination({ before: data.pokemons.pageInfo.startCursor, after: '' });
  }, [data]);

  const searchByName = useCallback((value: string) => {
    setName(value);
    setPagination(paginationEmptyValue);
  }, []);

  const searchByType = useCallback((value: string) => {
    setType(value);
    setPagination(paginationEmptyValue);
  }, []);

  useEffect(() => {
    if (data) {
      setTableData(
        data.pokemons.edges.map(({ node: { id, name, types, classification } }) => ({
          key: id,
          name,
          types,
          classification
        }))
      );
    }
  }, [data]);

  if (error) return <GenericErrorPage />;

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Row gutter={16}>
        <Col span={4} offset={6}>
          <div>Pokemon name</div>
          <Input.Search
            className="PokeomonList__input-name"
            data-testid="search-name"
            defaultValue={name}
            placeholder="Search by name"
            allowClear
            size="large"
            onSearch={searchByName}
          />
        </Col>
        <Col span={4}>
          <div>Pokemon type</div>
          <Input.Search
            className="PokeomonList__input-type"
            data-testid="search-type"
            defaultValue={name}
            placeholder="Filter by type"
            allowClear
            size="large"
            onSearch={searchByType}
          />
        </Col>
      </Row>
      <Row>
        <Col span={12} offset={6}>
          <Table
            columns={pokemonColumns}
            dataSource={tableData}
            pagination={false}
            loading={loading}
            data-testid="pokemon-table"
          ></Table>
        </Col>
      </Row>
      <Row>
        <Col span={4} offset={6} style={{ textAlign: 'start' }}>
          <Button
            disabled={!data || !data.pokemons.pageInfo.hasPrevPage}
            onClick={goPrevPage}
            data-testid="button-prev"
          >
            PREV PAGE
          </Button>
        </Col>
        <Col span={4} offset={4} style={{ textAlign: 'end' }}>
          <Button
            disabled={!data || !data.pokemons.pageInfo.hasNextPage}
            onClick={goNextPage}
            data-testid="button-next"
          >
            NEXT PAGE
          </Button>
        </Col>
      </Row>
    </Space>
  );
}

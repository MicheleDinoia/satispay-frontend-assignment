import React from 'react';
import { Tag } from 'antd';
import { getColorFromType } from '@root/utils/pokemons';

export type PokemonTypeProps = {
  type: string;
};

export default function PokemonType({ type }: PokemonTypeProps) {
  return <Tag color={getColorFromType(type)}>{type.toUpperCase()}</Tag>;
}

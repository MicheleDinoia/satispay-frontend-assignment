import { gql } from '@apollo/client';

export const POKEMONS_QUERY = gql`
  query GetPokemons($q: String, $after: ID, $before: ID, $limit: Int, $type: String) {
    pokemons(after: $after, before: $before, limit: $limit, q: $q, type: $type) {
      edges {
        node {
          id
          types
          name
          classification
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPrevPage
      }
    }
  }
`;
export const POKEMONS_BY_TYPE_QUERY = gql`
  query GetPokemonsByType($type: String!) {
    pokemonsByType(type: $type) {
      edges {
        node {
          id
          types
          name
          classification
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

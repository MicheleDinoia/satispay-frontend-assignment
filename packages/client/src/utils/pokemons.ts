export const getColorFromType = (type: string) => {
  switch (type.toLowerCase()) {
    case 'grass': {
      return '#78C850';
    }
    case 'fire': {
      return '#F08030';
    }
    case 'water': {
      return '#62bdff';
    }
    case 'ghost': {
      return '#705898';
    }
    case 'normal': {
      return '#A8A878';
    }
    case 'poison': {
      return '#A040A0';
    }
    case 'dragon': {
      return '#7038F8';
    }
    case 'ice': {
      return '#98D8D8';
    }
    case 'electric': {
      return '#F8D030';
    }
    case 'fairy': {
      return '#EE99AC';
    }
    case 'steel': {
      return '#B8B8D0';
    }
    case 'rook': {
      return '#B8A038';
    }
    case 'flying': {
      return '#A040A0';
    }
    case 'psychic': {
      return '#F85888';
    }
    case 'bug': {
      return '#A8B820';
    }
    case 'dark': {
      return '#705848';
    }
    case 'fighting': {
      return '#C03028';
    }
    default: {
      return '#68A090';
    }
  }
};

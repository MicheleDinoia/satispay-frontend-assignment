export type Pokemons = {
  id: string;
  name: string;
  types: string[];
  classification: string;
};

export type PageInfo = {
  startCursor: string;
  endCursor: string;
  hasNextPage: boolean;
  hasPrevPage: boolean;
};

export type Pokemon = {
  id: string;
  name: string;
  classification: string;
  types: [string];
};

export type PokemonEdge = {
  cursor: string;
  node: Pokemon;
};

export type PokemonsConnection = {
  edges: [PokemonEdge];
  pageInfo: PageInfo;
};

export type PokemonsQuery = {
  pokemons: {
    edges: [PokemonEdge];
    pageInfo: PageInfo;
  };
};

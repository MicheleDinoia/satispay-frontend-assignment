import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache, from, HttpLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { Layout, Space, Row, Col } from 'antd';
import AppHeader from '@components/AppHeader';
import AppFooter from '@components/AppFooter';
import AppContent from '@components/PokemonList';
import 'antd/dist/antd.css';

const errorLink = onError(({ graphQLErrors }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message }) => {
      console.log(`graphQL error: ${message}`);
    });
  }
});

const link = from([errorLink, new HttpLink({ uri: 'http://localhost:4000/' })]);

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Layout className="App" style={{ minHeight: '100vh' }}>
        <Space
          size="large"
          direction="vertical"
          style={{
            width: '100%',
            display: 'flex',
            minHeight: '100vh',
            flexDirection: 'column',
            justifyContent: 'space-between'
          }}
        >
          <AppHeader />
          <AppContent />
          <AppFooter />
        </Space>
      </Layout>
    </ApolloProvider>
  );
}

export default App;
